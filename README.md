Lead Management
============

Assumptions
------------
* Docker is installed on your machine
* .Net Core 3.1 is installed on your machine

Overview
------------

This repository contain the Lead Management components. It includes the following components:

* Server: The server component is created using .Net Core 3.1. The server component is structured using clean architecture and domain driven design. It exposes the jobs api as a REST service.
* Client: The client side component is created using Vue. It implements a UI that interacts with the server component in order to show the list of invited and accepted leads. It also exposes functionality for approving and/or declining leads.

The source code is available in Bitbucket and can be accessed via the following url:

* https://bitbucket.org/jakestateresa/leadmanagement

Server Component
------------

The server component is created using .Net Core 3.1. The server component is structured using clean architecture and domain driven design. It exposes the jobs api as a REST service.

XUnit tests are available to assert the following:

* HiPages.Application.Tests
    * GetJobQueryHandlerTests
        * ReturnsValueCorrectly
    * GetJobsQueryHandlerTests
        * ReturnsValueCorrectly
    * UpdateJobCommandHandlerTests
        * AppliesDiscountAndSendsMail
        * ThrowsExceptionWhenJobIsNotFound
        * UpdatesStatusAndSendsMail
* HiPages.Infrastructure.Tests
    * DummyMailSenderTests
        * MessagesAreLogged

Client Component
------------

The client side component is created using Vue. It implements a UI that interacts with the server component in order to show the list of invited and accepted leads. It also exposes functionality for approving and/or declining leads.

Unit tests are available to assert the following:

* App.vue
    * leads correctly
    * accepting invited lead moves it to the accepted list
    * declining invited lead removes it from the client side
* AcceptedLead.vue
    * renders correctly (44ms)
* InvitedLead.vue
    * renders correctly
    * triggers accept event on accept button click
    * triggers decline event on decline button click
* StringFilters
    * is can get initials for a givin string
    * is can format dates for display from an iso date
    * is can format dates for display from an iso current date
    * it can format ints as a currency

Running the Environment
------------

Docker compose file can be found on the root of this repository. In order to launch an instance of the end to end environment, run the following command on the terminal:

```console
docker-compose up --build
```

Note: The docker-compose file will open the port 8888 in your machine.

To access the client application:

 * Open your web browser
 * Navigate to http://localhost:8888

To access the api swagger console:

 * Open your web browser
 * Navigate to http://localhost:8888/api/jobs/docs/index.html