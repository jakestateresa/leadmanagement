using System;
using System.Globalization;
using HiPages.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace HiPages.Persistence
{
    public class HiPagesDbContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Suburb> Suburbs { get; set; }
        public DbSet<Job> Jobs { get; set; }

        private readonly string connectionString;
        public HiPagesDbContext(IOptions<DbSettings> settings)
        {
            connectionString = settings?.Value.ConnectionString ?? throw new ArgumentNullException(nameof(settings));
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Suburb>(b =>
            {
                b.ToTable("suburbs");
                b.HasKey(e => e.Id)
                    .HasName("id");
                b.Property(e => e.Name)
                    .HasColumnName("name")
                    .IsRequired();
                b.Property(e => e.PostCode)
                    .HasColumnName("postcode")
                    .IsRequired();
            });
            
            modelBuilder.Entity<Category>(b =>
            {
                b.ToTable("categories");
                b.HasKey(e => e.Id)
                    .HasName("id");
                b.Property(e => e.Name)
                    .HasColumnName("name")
                    .IsRequired();
                
                b.Property(e => e.ParentCategoryId)
                    .HasColumnName("parent_category_id");

                b.HasOne(e => e.ParentCategory)
                    .WithMany(c => c.SubCategories)
                    .HasForeignKey(c => c.ParentCategoryId);
            });
            
            modelBuilder.Entity<Job>(b =>
            {
                b.ToTable("jobs");
                b.HasKey(e => e.Id)
                    .HasName("id");
                b.Property(e => e.Name)
                    .HasColumnName("contact_name")
                    .IsRequired();
                b.Property(e => e.Phone)
                    .HasColumnName("contact_name")
                    .IsRequired();
                b.Property(e => e.Email)
                    .HasColumnName("contact_email")
                    .IsRequired();
                b.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsRequired();
                b.Property(e => e.Price)
                    .HasColumnName("price")
                    .IsRequired();

                b.Property(e => e.Status)
                    .HasColumnName("status")
                    .ValueGeneratedOnAdd()
                    .HasConversion(
                        v => v.ToString().ToLowerInvariant(),
                        v => Enum.Parse<Status>(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(v)));
                    
                b.Property(e => e.CategoryId)
                    .HasColumnName("category_id");
                
                b.HasOne(e => e.Category)
                    .WithMany(c => c.Jobs)
                    .HasForeignKey(e => e.CategoryId)
                    .HasConstraintName("fk_jobs_category");

                b.Property(e => e.SuburbId)
                    .HasColumnName("suburb_id");
                
                b.HasOne(e => e.Suburb)
                    .WithMany(s => s.Jobs)
                    .HasForeignKey(e => e.SuburbId)
                    .HasConstraintName("fk_jobs_suburb");

                b.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .ValueGeneratedOnAdd();

                b.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .ValueGeneratedOnUpdate();
            });


        }
    }
}