using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using HiPages.Application.Repositories;
using HiPages.Domain;
using Microsoft.EntityFrameworkCore;

namespace HiPages.Persistence
{
    public class JobRepository : IJobRepository
    {
        private readonly HiPagesDbContext dbContext;
        public JobRepository(HiPagesDbContext dbContext)
        {
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }
        
        public async Task<Job> GetByIdAsync(int key, CancellationToken cancellationToken)
        {
            return await dbContext.Jobs
                .Where(j => j.Id == key)
                .SingleOrDefaultAsync(cancellationToken);
        }

        public async Task<List<Job>> GetAllAsync(Expression<Func<Job, bool>> predicate, CancellationToken cancellationToken)
        {
            return await dbContext.Jobs
                .Where(predicate)
                .Include(j => j.Suburb)
                .Include(j => j.Category)
                .ToListAsync(cancellationToken);
        }

        public async Task UpdateAsync(Job model, CancellationToken cancellationToken)
        {
            dbContext.Jobs.Update(model);
            await dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}