using System;
using System.Threading;
using System.Threading.Tasks;
using HiPages.Application.Repositories;
using HiPages.Application.Services;
using HiPages.Domain;
using MediatR;

namespace HiPages.Application.Commands
{
    public class UpdateJobCommand : IRequest
    {
        public int Id { get; set; }
        public Status Status { get; set; }       

        public class UpdateJobCommandHandler : IRequestHandler<UpdateJobCommand>
        {
            private readonly IJobRepository repository;
            private readonly IMailSender mailSender;
            public UpdateJobCommandHandler(IJobRepository repository, IMailSender mailSender)
            {
                this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
                this.mailSender = mailSender ?? throw new ArgumentNullException(nameof(mailSender));
            }
            
            public async Task<Unit> Handle(UpdateJobCommand request, CancellationToken cancellationToken)
            {
                var job = await repository.GetByIdAsync(request.Id, cancellationToken);
                if (job == null)
                {
                    throw new Exception($"Unable to find the job with id {request.Id}");
                }

                if (job.Price > 500)
                {
                    job.Price = (int)Math.Round(job.Price * .9, 0);
                }
                
                job.Status = request.Status;
                await repository.UpdateAsync(job, cancellationToken);
                
                mailSender.SendEmail("noreplay@hipages.com", "sales@techtest.com", $"Job ID: {job.Id} has been accepted.");

                return Unit.Value;
            }
        }
    }
}