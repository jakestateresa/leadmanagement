using HiPages.Domain;

namespace HiPages.Application.Repositories
{
    public interface IJobRepository : IReadWriteRepository<Job, int>
    {
        
    }
}