using System.Threading;
using System.Threading.Tasks;

namespace HiPages.Application.Repositories
{
    public interface IReadWriteRepository<TModel, TValue> : IReadOnlyRepository<TModel, TValue>
    {
        public Task UpdateAsync(TModel model, CancellationToken cancellationToken);
    }
}