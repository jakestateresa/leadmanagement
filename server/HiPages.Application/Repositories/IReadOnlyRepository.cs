using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace HiPages.Application.Repositories
{
    public interface IReadOnlyRepository<TModel, TKey>
    {
        Task<TModel> GetByIdAsync(TKey key, CancellationToken cancellationToken);
        Task<List<TModel>> GetAllAsync(Expression<Func<TModel, bool>> predicate, CancellationToken cancellationToken);
    }
}