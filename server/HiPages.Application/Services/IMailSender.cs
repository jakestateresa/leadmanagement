namespace HiPages.Application.Services
{
    public interface IMailSender
    {
        void SendEmail(string from, string to, string message);
    }
}