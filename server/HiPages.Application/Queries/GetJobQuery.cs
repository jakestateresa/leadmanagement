using System;
using System.Threading;
using System.Threading.Tasks;
using HiPages.Application.Repositories;
using HiPages.Domain;
using MediatR;

namespace HiPages.Application.Queries
{
    public class GetJobQuery : IRequest<Job>
    {
        public int Id { get; set; }       
        
        public class GetJobQueryHandler : IRequestHandler<GetJobQuery, Job>
        {
            private readonly IJobRepository repository;
            public GetJobQueryHandler(IJobRepository repository)
            {
                this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
            }
            
            public Task<Job> Handle(GetJobQuery request, CancellationToken cancellationToken)
            {
                return repository.GetByIdAsync(request.Id, cancellationToken);
            }
        }
    }
}