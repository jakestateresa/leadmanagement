using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using HiPages.Application.Repositories;
using HiPages.Domain;
using MediatR;

namespace HiPages.Application.Queries
{
    public class GetJobsQuery : IRequest<List<Job>>
    {
        public Status? Status { get; set; }       
        
        public class GetJobsQueryHandler : IRequestHandler<GetJobsQuery, List<Job>>
        {
            private readonly IJobRepository repository;
            public GetJobsQueryHandler(IJobRepository repository)
            {
                this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
            }
            
            public Task<List<Job>> Handle(GetJobsQuery request, CancellationToken cancellationToken)
            {
                if (request.Status == null)
                {
                    return repository.GetAllAsync(job => true, cancellationToken);
                }
                return repository.GetAllAsync(job => job.Status == request.Status, cancellationToken);
            }
        }
    }
}