using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace HiPages.Infrastructure.Tests
{
    public class DummyMailSenderTests
    {
        private DummyMailSender mailSender;
        private Mock<AbstractLogger<DummyMailSender>> logger;

        public DummyMailSenderTests()
        {
            logger = new Mock<AbstractLogger<DummyMailSender>>(MockBehavior.Strict);
            mailSender = new DummyMailSender(logger.Object);
        }
        
        [Fact]
        public void MessagesAreLogged()
        {
            logger.Setup(x => x.Log(LogLevel.Information, 
                    null, 
                    It.IsAny<string>()));
            
            mailSender.SendEmail("noreplay@hipages.com", "sales@techtest.com", "Job ID: 123456 has been accepted.");

            logger.Verify(x => x.Log(LogLevel.Information, 
                null, 
                "Sending email from 'noreplay@hipages.com' to 'sales@techtest.com' containing message: Job ID: 123456 has been accepted."), 
            Times.Once);

        }
    }
}