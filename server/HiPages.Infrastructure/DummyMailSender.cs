﻿using System;
using HiPages.Application.Services;
using Microsoft.Extensions.Logging;

namespace HiPages.Infrastructure
{
    public class DummyMailSender : IMailSender
    {
        private readonly ILogger<DummyMailSender> logger;
        public DummyMailSender(ILogger<DummyMailSender> logger)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        
        public void SendEmail(string @from, string to, string message)
        {
            logger.LogInformation($"Sending email from '{from}' to '{to}' containing message: {message}");
        }
    }
}