using AutoMapper;
using HiPages.Application.Queries;
using HiPages.Application.Repositories;
using HiPages.Application.Services;
using HiPages.Infrastructure;
using HiPages.Persistence;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NSwag.AspNetCore;

namespace HiPages.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("CorsAllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));
            
            services.AddOptions();
            services
                .Configure<DbSettings>(Configuration.GetSection("Database"));

            services.AddControllers();
                
            services.AddSwaggerDocument(); 
            
            services.AddMediatR(typeof(GetJobsQuery).Assembly);
            services.AddAutoMapper(GetType().Assembly);
            
            services.AddDbContext<HiPagesDbContext>();
            services.AddScoped<IJobRepository, JobRepository>();
            services.AddScoped<IMailSender, DummyMailSender>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("CorsAllowAll");
            app.UseStaticFiles();
            app.UseOpenApi();
            app.UseSwaggerUi3(); 
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            
            app.UseSwaggerUi3(settings =>
            {
                if (env.IsDevelopment())
                {
                    settings.SwaggerRoutes.Add(new SwaggerUi3Route("root", "/jobs/docs/api-specification.json"));
                }
                else
                {
                    //make it work in nginx virtual directory
                    settings.SwaggerRoutes.Add(new SwaggerUi3Route("root", "/api/jobs/docs/api-specification.json"));
                }
                settings.Path = "/jobs/docs";
            });

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}