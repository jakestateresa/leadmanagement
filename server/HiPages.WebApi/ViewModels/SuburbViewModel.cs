namespace HiPages.WebApi.ViewModels
{
    public class SuburbViewModel
    {
        public string Name { get; set; }
        public string PostCode { get; set; }
    }
}