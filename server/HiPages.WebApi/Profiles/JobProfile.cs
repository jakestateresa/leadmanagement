using System;
using System.Collections.Generic;
using AutoMapper;
using HiPages.Domain;
using HiPages.WebApi.ViewModels;

namespace HiPages.WebApi.Profiles
{
    public class JobProfile : Profile
    {
        public JobProfile()
        {
            Func<Job, string> mapCategory = (Job j) =>
            {
                var returnValue = new List<string>();
                Category category = null;
                do
                {
                    category = category == null ? j.Category : category.ParentCategory;
                    returnValue.Add(category.Name);
                } while (category.ParentCategory != null);

                returnValue.Reverse();
                return string.Join('/', returnValue);
            };

            CreateMap<Job, JobViewModel>()
                .ForMember(d => d.Category,
                    opt => opt.MapFrom(s => mapCategory(s)))
                .ForMember(d => d.CreatedAt, 
                    opt => opt.MapFrom(s => s.CreatedAt.ToString("yyyy-MM-ddTHH:mm:ss.fffffffzzz")))
                .ForMember(d => d.UpdatedAt, 
                opt => opt.MapFrom(s => s.UpdatedAt.Value == DateTime.MinValue ? null :  s.UpdatedAt.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffffffzzz")));
        }
    }
}