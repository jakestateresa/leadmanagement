using AutoMapper;
using HiPages.Domain;
using HiPages.WebApi.ViewModels;

namespace HiPages.WebApi.Profiles
{
    public class SuburbProfile : Profile
    {
        public SuburbProfile()
        {
            CreateMap<Suburb, SuburbViewModel>();
        }
    }
}