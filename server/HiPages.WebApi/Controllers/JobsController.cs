using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using AutoMapper;
using HiPages.Application.Commands;
using HiPages.Application.Queries;
using HiPages.Domain;
using HiPages.WebApi.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace HiPages.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JobsController
    {
        private readonly IMediator mediator;
        private readonly IMapper mapper;
        public JobsController(IMediator mediator, IMapper mapper)
        {
            this.mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        
        [HttpGet]
        public async Task<List<JobViewModel>> GetJobs([FromQuery]string status = "new")
        {
            var query = new GetJobsQuery
            {
                Status = string.IsNullOrEmpty(status) ?  (Status?)null : Enum.Parse<Status>(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(status))
            };

            var jobs = await mediator.Send(query);
            var returnValue = mapper.Map<List<JobViewModel>>(jobs);
            return returnValue;
        }
        
        [HttpGet("{id}")]
        public async Task<JobViewModel> GetJob([FromRoute]int id)
        {
            var query = new GetJobQuery()
            {
                Id = id,
            };

            var job = await mediator.Send(query);
            var returnValue = mapper.Map<JobViewModel>(job);
            return returnValue;
        }
        
        [HttpPost("{id}/approve")]
        public async void ApproveJob([FromRoute]int id)
        {
            var query = new UpdateJobCommand()
            {
                Id = id,
                Status = Status.Approved
            };

            await mediator.Send(query);
        }
        
        [HttpPost("{id}/decline")]
        public async void DeclineJob([FromRoute]int id)
        {
            var query = new UpdateJobCommand()
            {
                Id = id,
                Status = Status.Declined
            };

            await mediator.Send(query);
        }
    }
}