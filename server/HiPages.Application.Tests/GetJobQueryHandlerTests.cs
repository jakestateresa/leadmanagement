using System.Threading;
using System.Threading.Tasks;
using HiPages.Application.Queries;
using HiPages.Application.Repositories;
using HiPages.Domain;
using Moq;
using Xunit;

namespace HiPages.Application.Tests
{
    public class GetJobQueryHandlerTests
    {
        private readonly Mock<IJobRepository> repository;
      
        public GetJobQueryHandlerTests()
        {
            repository = new Mock<IJobRepository>(MockBehavior.Strict);
        }
        
        [Fact]
        public async void ReturnsValueCorrectly()
        {
            repository.
                Setup(m => m.GetByIdAsync(It.IsAny<int>(), CancellationToken.None))
                .Returns(Task.FromResult<Job>(new Job
                    {
                        Id = 1212,
                        Price = 100,
                        Status = Status.New
                    }))
                .Verifiable();
            
            var command = new GetJobQuery.GetJobQueryHandler(repository.Object);
            
            var returnValue = await command.Handle(new GetJobQuery()
            {
                Id = 1212,
            }, CancellationToken.None);

            Assert.Equal(returnValue.Id, 1212);
            Assert.Equal(returnValue.Price, 100);
            Assert.Equal(returnValue.Status, Status.New);
            
            repository.VerifyAll();
        }
    }
}