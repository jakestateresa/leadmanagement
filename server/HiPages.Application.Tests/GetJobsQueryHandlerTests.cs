using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using HiPages.Application.Queries;
using HiPages.Application.Repositories;
using HiPages.Domain;
using Moq;
using Xunit;

namespace HiPages.Application.Tests
{
    public class GetJobsQueryHandlerTests
    {
        private readonly Mock<IJobRepository> repository;
      
        public GetJobsQueryHandlerTests()
        {
            repository = new Mock<IJobRepository>(MockBehavior.Strict);
        }
        
        [Fact]
        public async void ReturnsValueCorrectly()
        {
            repository.
                Setup(m => m.GetAllAsync(It.IsAny<Expression<Func<Job, bool>>>(), CancellationToken.None))
                .Returns(Task.FromResult<List<Job>>(new List<Job>
                {
                    new Job
                    {
                        Id = 1212,
                        Price = 100,
                        Status = Status.New
                    }
                }))
                .Verifiable();
            
            var command = new GetJobsQuery.GetJobsQueryHandler(repository.Object);
            
            var returnValue = await command.Handle(new GetJobsQuery()
            {
                Status = Status.New,
            }, CancellationToken.None);

            Assert.Equal(returnValue.Count, 1);
            Assert.Equal(returnValue[0].Id, 1212);
            Assert.Equal(returnValue[0].Price, 100);
            Assert.Equal(returnValue[0].Status, Status.New);
            
            repository.VerifyAll();
        }
    }
}