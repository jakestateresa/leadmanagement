using System;
using System.Threading;
using System.Threading.Tasks;
using HiPages.Application.Commands;
using HiPages.Application.Repositories;
using HiPages.Application.Services;
using HiPages.Domain;
using Moq;
using Xunit;

namespace HiPages.Application.Tests
{
    public class UpdateJobCommandHandlerTests
    {
        private readonly Mock<IJobRepository> repository;
        private readonly Mock<IMailSender> mailSender;

        public UpdateJobCommandHandlerTests()
        {
            repository = new Mock<IJobRepository>(MockBehavior.Strict);
             mailSender = new Mock<IMailSender>(MockBehavior.Strict);
        }
        
        [Fact]
        public async void UpdatesStatusAndSendsMail()
        {
            repository.
                Setup(m => m.GetByIdAsync(It.IsAny<int>(), CancellationToken.None))
                .Returns(Task.FromResult<Job>(new Job
                    {
                        Id = 1212,
                        Price = 100,
                        Status = Status.New
                    }))
                .Verifiable();
            
            repository.
                Setup(m => m.UpdateAsync(It.IsAny<Job>(), CancellationToken.None))
                .Callback<Job, CancellationToken>((job, cancellationToken) =>
                {
                    Assert.Equal(job.Status, Status.Approved);
                })
                .Returns(Task.CompletedTask)
                .Verifiable();
            
            mailSender.
                Setup(m => m.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Verifiable();
               
            var command = new UpdateJobCommand.UpdateJobCommandHandler(repository.Object, mailSender.Object);
            
            await command.Handle(new UpdateJobCommand
            {
                Id = 1212,
                Status = Status.Approved,
            }, CancellationToken.None);

            repository.VerifyAll();
            mailSender.VerifyAll();
        }
        
        [Fact]
        public async void AppliesDiscountAndSendsMail()
        {
            repository.
                Setup(m => m.GetByIdAsync(It.IsAny<int>(), CancellationToken.None))
                .Returns(Task.FromResult<Job>(new Job
                {
                    Id = 1212,
                    Price = 1000,
                    Status = Status.New
                }))
                .Verifiable();
            
            repository.
                Setup(m => m.UpdateAsync(It.IsAny<Job>(), CancellationToken.None))
                .Callback<Job, CancellationToken>((job, cancellationToken) =>
                {
                    Assert.Equal(job.Price, 900);
                })
                .Returns(Task.CompletedTask)
                .Verifiable();
            
            mailSender.
                Setup(m => m.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Verifiable();
               
            var command = new UpdateJobCommand.UpdateJobCommandHandler(repository.Object, mailSender.Object);
            
            await command.Handle(new UpdateJobCommand
            {
                Id = 1212,
                Status = Status.Approved,
            }, CancellationToken.None);

            repository.VerifyAll();
            mailSender.VerifyAll();
        }
        
        [Fact]
        public async void ThrowsExceptionWhenJobIsNotFound()
        {
            repository.
                Setup(m => m.GetByIdAsync(It.IsAny<int>(), CancellationToken.None))
                .Returns(Task.FromResult<Job>(null))
                .Verifiable();
            
            var command = new UpdateJobCommand.UpdateJobCommandHandler(repository.Object, mailSender.Object);
            
            var exception = await Assert.ThrowsAsync<Exception>(async () =>
            {
                await command.Handle(new UpdateJobCommand
                {
                    Id = 1212,
                    Status = Status.Approved,
                }, CancellationToken.None);
            });
            Assert.Equal("Unable to find the job with id 1212", exception.Message);
            
            repository.VerifyAll();
            mailSender.VerifyAll();
        }
    }
}