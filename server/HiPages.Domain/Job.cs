﻿using System;

namespace HiPages.Domain
{
    public class Job
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public Status Status { get; set; }
        public int SuburbId { get; set; }
        public Suburb Suburb { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}