using System.Collections.Generic;

namespace HiPages.Domain
{
    public class Suburb
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PostCode { get; set; }

        public ICollection<Job> Jobs { get; set; }
    }
}