using System.Collections.Generic;

namespace HiPages.Domain
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public int ParentCategoryId { get; set; }
        public Category ParentCategory { get; set; }

        public ICollection<Job> Jobs { get; set; }
        
        public ICollection<Category> SubCategories { get; set; }
    }
}