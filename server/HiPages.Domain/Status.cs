namespace HiPages.Domain
{
    public enum Status
    {
        New,
        Approved,
        Declined
    }
}