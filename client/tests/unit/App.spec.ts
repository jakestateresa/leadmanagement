import Vue from 'vue';
import Vuetify from 'vuetify';

import chai, { expect } from 'chai';
import spies from 'chai-spies';
import { shallowMount } from '@vue/test-utils';

import App from '@/App.vue';
import Lead from '@/models/Lead';
import LeadsService from '@/services/LeadsService';

chai.use(spies);
Vue.config.productionTip = false;
Vue.use(Vuetify);

const leads: Lead[] = [
  {
    id: 1001,
    name: 'Jhonny Bruno',
    email: 'johnny@bruno.com',
    phone: '0478 123 456',
    price: 456,
    status: 'New',
    category: 'Home Improvement',
    suburb: {
      name: 'Hornsby',
      postCode: '2077',
    },
    description: 'Quisque blandit erat id mi tincidunt porta. Vivamus eleifend sagittis neque id maximus.',
    createdAt: '2011-10-05T14:48:00.000Z',
    updatedAt: '2011-10-10T14:48:00.000Z',
  },
  {
    id: 1002,
    name: 'Jhonny Bravo',
    email: 'johnny@bravo.com',
    phone: '0478 654 321',
    price: 654,
    status: 'New',
    category: 'Carpentry',
    suburb: {
      name: 'Oran Park',
      postCode: '2570',
    },
    description: 'Quisque blandit erat id mi tincidunt porta. Vivamus eleifend sagittis neque id maximus.',
    createdAt: '2011-10-05T14:48:00.000Z',
    updatedAt: '2011-10-10T14:48:00.000Z',
  },
];

describe('App.vue', () => {
  let leadsService: LeadsService;

  before(() => {
    leadsService = new LeadsService();

    chai.spy.on(leadsService, 'getAll', (status) => {
      if (status === 'new') {
        return leads;
      }

      return [];
    });

    chai.spy.on(leadsService, 'approve');
    chai.spy.on(leadsService, 'decline');
  });

  it('leads correctly', async () => {
    const wrapper = shallowMount(App);

    wrapper.vm.leadsService = leadsService;

    expect(wrapper.vm.invited).eql([]);
    expect(wrapper.vm.accepted).eql([]);

    await wrapper.vm.loadLeads();
    expect(leadsService.getAll).to.have.been.called.exactly(2);

    expect(wrapper.vm.invited).eql(leads);
    expect(wrapper.vm.accepted).eql([]);
  });

  it('accepting invited lead moves it to the accepted list', async () => {
    const wrapper = shallowMount(App);
    wrapper.vm.leadsService = leadsService;

    await wrapper.vm.loadLeads();

    const leadToApprove = wrapper.vm.invited[0];
    expect(wrapper.vm.invited).contains(leadToApprove);
    expect(wrapper.vm.accepted).not.contains(leadToApprove);

    wrapper.vm.accept(leadToApprove);
    expect(leadsService.approve).to.have.been.called();

    expect(wrapper.vm.invited).not.contains(leadToApprove);
    expect(wrapper.vm.accepted).contains(leadToApprove);
  });


  it('declining invited lead removes it from the client side', async () => {
    const wrapper = shallowMount(App);
    wrapper.vm.leadsService = leadsService;

    await wrapper.vm.loadLeads();

    const leadToApprove = wrapper.vm.invited[0];
    expect(wrapper.vm.invited).contains(leadToApprove);
    expect(wrapper.vm.accepted).not.contains(leadToApprove);

    wrapper.vm.decline(leadToApprove);
    expect(leadsService.decline).to.have.been.called();

    expect(wrapper.vm.invited).not.contains(leadToApprove);
    expect(wrapper.vm.accepted).not.contains(leadToApprove);
  });
});
