import Vue from 'vue';
import Vuetify from 'vuetify';

import chai, { expect } from 'chai';
import spies from 'chai-spies';
import { mount } from '@vue/test-utils';

import InvitedLead from '@/components/InvitedLead.vue';
import Lead from '@/models/Lead';

chai.use(spies);

describe('InvitedLead.vue', () => {
  Vue.use(Vuetify);
  Vue.config.productionTip = false;

  const lead: Lead = {
    id: 1001,
    name: 'Jhonny Bruno',
    email: 'johnny@bruno.com',
    phone: '0478 123 456',
    price: 456,
    status: 'Approved',
    category: 'Home Improvement',
    suburb: {
      name: 'Hornsby',
      postCode: '2077',
    },
    description: 'Quisque blandit erat id mi tincidunt porta. Vivamus eleifend sagittis neque id maximus.',
    createdAt: '2011-10-05T14:48:00.000Z',
    updatedAt: '2011-10-10T14:48:00.000Z',
  };

  it('renders correctly', () => {
    const wrapper = mount(InvitedLead, {
      propsData: { lead },
    });

    const title = wrapper.find('.v-list-item__title');
    expect(title.text()).eq(lead.name);

    const subtitle = wrapper.find('.v-list-item__subtitle');
    expect(subtitle.text()).eq('October 5 2011 @ 2:48 pm');

    const details = wrapper.findAll('.detail');
    expect(details.length).eq(3);
    expect(details.at(0).text()).contains('Hornsby 2077');
    expect(details.at(1).text()).contains('Home Improvement');
    expect(details.at(2).text()).eq('Job ID: 1001');

    const priceAndLeadInvitation = wrapper.find('.v-card__actions > .v-card__text');
    expect(priceAndLeadInvitation.text()).eq('$456.00 Lead Invitation');

    const description = wrapper.find('.description');
    expect(description.text()).eq(lead.description);
  });

  it('triggers accept event on accept button click', () => {
    const wrapper = mount(InvitedLead, {
      propsData: { lead },
    });

    const acceptEvent = chai.spy();
    wrapper.vm.$on('accept', acceptEvent);

    const acceptButton = wrapper.find('.accept-button');
    acceptButton.trigger('click');

    const declineEvent = chai.spy();
    wrapper.vm.$on('decline', declineEvent);

    expect(acceptEvent).to.have.been.called();
    expect(declineEvent).not.to.have.been.called();
  });

  it('triggers decline event on decline button click', () => {
    const wrapper = mount(InvitedLead, {
      propsData: { lead },
    });

    const acceptEvent = chai.spy();
    wrapper.vm.$on('accept', acceptEvent);

    const declineEvent = chai.spy();
    wrapper.vm.$on('decline', declineEvent);

    const declineButton = wrapper.find('.decline-button');
    declineButton.trigger('click');

    expect(acceptEvent).not.to.have.been.called();
    expect(declineEvent).to.have.been.called();
  });
});
