import Vue from 'vue';
import Vuetify from 'vuetify';

import { expect } from 'chai';
import { mount } from '@vue/test-utils';

import AcceptedLead from '@/components/AcceptedLead.vue';
import Lead from '@/models/Lead';

describe('AcceptedLead.vue', () => {
  Vue.use(Vuetify);
  Vue.config.productionTip = false;

  const lead: Lead = {
    id: 1001,
    name: 'Jhonny Bruno',
    email: 'johnny@bruno.com',
    phone: '0478 123 456',
    price: 456,
    status: 'Approved',
    category: 'Home Improvement',
    suburb: {
      name: 'Hornsby',
      postCode: '2077',
    },
    description: 'Quisque blandit erat id mi tincidunt porta. Vivamus eleifend sagittis neque id maximus.',
    createdAt: '2011-10-05T14:48:00.000Z',
    updatedAt: '2011-10-10T14:48:00.000Z',
  };

  it('renders correctly', () => {
    const wrapper = mount(AcceptedLead, {
      propsData: { lead },
    });

    const title = wrapper.find('.v-list-item__title');
    expect(title.text()).eq(lead.name);

    const subtitle = wrapper.find('.v-list-item__subtitle');
    expect(subtitle.text()).eq('October 5 2011 @ 2:48 pm');

    const details = wrapper.findAll('.detail');
    expect(details.length).eq(6);
    expect(details.at(0).text()).contains('Hornsby 2077');
    expect(details.at(1).text()).contains('Home Improvement');
    expect(details.at(2).text()).eq('Job ID: 1001');
    expect(details.at(3).text()).eq('$456.00 Lead Invitation');
    expect(details.at(4).text()).contains('0478 123 456');
    expect(details.at(5).text()).contains('johnny@bruno.com');

    const detailText = wrapper.findAll('.detail-text');
    expect(detailText.length).eq(2);
    expect(detailText.at(0).text()).contains('0478 123 456');
    expect(detailText.at(1).text()).eq('johnny@bruno.com');

    const description = wrapper.find('.description');
    expect(description.text()).eq(lead.description);
  });
});
