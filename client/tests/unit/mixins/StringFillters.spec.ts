import { expect } from 'chai';
import moment from 'moment';

import StringFilters from '../../../src/mixins/StringFilters';


describe('StringFilters', () => {
  const stringFilters = StringFilters;

  //   formatCurrency(value: number) {
  //     return value.toFixed(2);
  //   },
  // },

  it('is can get initials for a givin string', () => {
    const name = 'Jose Bruno';
    const initials = 'J';
    expect(stringFilters.filters.initials(name)).eq(initials);
  });

  it('is can format dates for display from an iso date', () => {
    const isoDate = '2011-10-05T14:48:00.000Z';
    const displayDate = 'October 5 2011 @ 2:48 pm';
    expect(stringFilters.filters.formatDate(isoDate)).eq(displayDate);
  });

  it('is can format dates for display from an iso current date', () => {
    const currentDate = moment();
    const isoDate = currentDate.toISOString();
    const displayDate = currentDate.utc().format('MMMM D @ h:mm a');
    expect(stringFilters.filters.formatDate(isoDate)).eq(displayDate);
  });

  it('it can format ints as a currency', () => {
    const amount = 123;
    expect(stringFilters.filters.formatCurrency(amount)).eq('123.00');
  });
});
