import {
  mdiMapMarker,
  mdiBriefcase,
  mdiPhone,
  mdiEmail,
} from '@mdi/js';

const IconsData = {
  data: () => ({
    icons: {
      mapMarker: mdiMapMarker,
      briefcase: mdiBriefcase,
      phone: mdiPhone,
      email: mdiEmail,
    },
  }),
};
export default IconsData;
