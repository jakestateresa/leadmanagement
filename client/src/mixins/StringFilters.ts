import moment from 'moment';

const StringFilters = {
  filters: {
    formatDate(value: string) {
      const now = moment();
      const date = moment(value);

      return date.utc().format(`MMMM D ${now.isSame(date, 'year') ? '' : 'YYYY '}@ h:mm a`);
    },
    initials(value: string) {
      return value.charAt(0);
    },
    formatCurrency(value: number) {
      return value.toFixed(2);
    },
  },
};
export default StringFilters;
