import axios from 'axios';
import Lead from '@/models/Lead';

class LeadsService {
  apiPrefix: string;

  constructor() {
    this.apiPrefix = process.env.VUE_APP_API_PREFIX;
  }

  async getAll(status: string) : Promise<Lead[]> {
    const response = await axios.get<Array<Lead>>(`${this.apiPrefix}/api/jobs?status=${status}`);
    return response.data;
  }

  async approve(lead: Lead) {
    await axios.post(`${this.apiPrefix}/api/jobs/${lead.id}/decline`);
  }

  async decline(lead: Lead) {
    await axios.post(`${this.apiPrefix}/api/jobs/${lead.id}/decline`);
  }
}

export default LeadsService;
