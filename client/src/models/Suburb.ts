export default interface Suburb
{
    name: string,
    postCode: string,
}
