
import Suburb from './Suburb';

export default interface Lead {
  id: number,
  name: string,
  phone: string,
  email: string,
  description: string,
  price: number,
  status: string,
  suburb: Suburb,
  category: string,
  createdAt: string,
  updatedAt: string,
}
